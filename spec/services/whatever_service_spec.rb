require_relative '../../services/whatever_service.rb'

describe WhateverService do
  describe '.new' do
    context 'when parameters is invalid' do
      it 'raise Error when name is nil' do
        expect { described_class.new(name: nil, age: 12) }.to raise_error(
          WhateverService::NotEnoughDataProvidedError
        )
      end

      it 'raise Error when age is nil' do
        expect { described_class.new(name: 'Name', age: nil) }.to raise_error(
          WhateverService::NotEnoughDataProvidedError
        )
      end

      it 'raise Error when both is nil' do
        expect { described_class.new(name: nil, age: nil) }.to raise_error(
          WhateverService::NotEnoughDataProvidedError
        )
      end

      it 'raise Error when age is negative' do
        expect { described_class.new(name: 'Chris', age: -1) }.to raise_error(
          WhateverService::InvalidAgeProvidedError
        )
      end

      it 'raise Error when age is not a number' do
        expect { described_class.new(name: 'Chris', age: 'a') }.to raise_error(
          WhateverService::InvalidAgeProvidedError
        )
      end
    end
  end

  context 'With valid instance' do
    let(:brienne) { described_class.new(name: 'Brienne of Tarth', age: 38) }
    let(:little_girl) { described_class.new(name: 'Arya', age: 10) }
    let(:baby) { described_class.new(name: 'Sam', age: 2) }
    let(:my_queen) do
      described_class.new(
        name: 'Khaleesi The Queen Across the Water',
        age: 30
      )
    end

    describe '#first_name' do
      it 'return valid first name' do
        expect(brienne.first_name).to eq('Brienne')
      end
    end

    describe '#last_name' do
      it 'return valid last name' do
        expect(brienne.last_name).to eq('Tarth')
      end
    end

    describe '#big_name?' do
      it 'return true with name.size > 5' do
        expect(my_queen.big_name?).to be_truthy
      end

      it 'return false with name.size <= 5' do
        expect(brienne.big_name?).to be_falsy
      end
    end

    describe '#birth_year' do
      it 'return valid birth_year' do
        expect(brienne.birth_year).to eq(Time.now.year - brienne.age)
      end
    end

    describe '#can_go_to_jail?' do
      it 'return true with age < 18' do
        expect(little_girl.can_go_to_jail?).to be_falsy
      end

      it 'return true with age >= 18' do
        expect(brienne.can_go_to_jail?).to be_truthy
      end
    end

    describe '#age_range' do
      it 'less then < 10' do
        expect(baby.age_range).to eq('less then 10')
      end

      it 'teenager when age >= 10' do
        expect(little_girl.age_range).to eq('teenager')
      end

      it 'over 18 when > 18' do
        expect(brienne.age_range).to eq('over 18')
      end
    end

    describe '#weather' do
      before do
        response = {
          weather: [
            {
              id: 300,
              main: 'Drizzle',
              description: 'light intensity drizzle',
              icon: '09d'
            }
          ]
        }

        stub_request(
          :get,
          described_class::WEATHER_URL
        ).to_return(status: 200, body: response.to_json, headers: {})
      end

      it 'weather valid' do
        expect(baby.weather).to eq('Drizzle')
      end
    end

    describe '#random_number' do
      let!(:random) do
        class_double('Random').as_stubbed_const(
          transfer_nested_constants: true
        )
      end

      it 'return random number' do
        allow(random).to receive(:rand) { 100 }
        expect(baby.random_number).to eq(100)
      end

      it 'call Random.rand' do
        expect(random).to receive(:rand) { 100 }
        baby.random_number
      end
    end
  end
end
