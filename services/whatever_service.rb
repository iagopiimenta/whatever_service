require 'uri'
require 'active_support/core_ext/object/blank'
require 'net/http'

class WhateverService
  NotEnoughDataProvidedError = Class.new(StandardError)
  InvalidAgeProvidedError = Class.new(StandardError)

  WEATHER_URL = URI('http://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b1b15e88fa797225412429c1c50c122a1')

  attr_accessor :name, :age

  def initialize(name:, age:)
    raise NotEnoughDataProvidedError if name.blank? || age.blank?
    raise InvalidAgeProvidedError if !age.is_a?(Integer) || age <= 0

    self.name = name
    self.age = age
  end

  def first_name
    name.split(' ').first
  end

  def last_name
    name.split(' ').last
  end

  def big_name?
    name.split(' ').length > 5
  end

  def birth_year
    DateTime.now.year - age
  end

  def can_go_to_jail?
    age >= 18
  end

  def age_range
    if age < 10
      'less then 10'
    elsif age >= 10 && age < 18
      'teenager'
    else
      'over 18'
    end
  end

  def weather
    response = Net::HTTP.get(WEATHER_URL)
    JSON.parse(response)['weather'].first['main']
  end

  def random_number
    Random.rand
  end
end
